# BEING A DEVELOPER ROCKS
>> Learn it, live it, love it!
[![forthebadge](https://forthebadge.com/images/badges/fuck-it-ship-it.svg)](https://forthebadge.com) 
## Website
### Being a dev rocks - i'll tell you why!
> It's just my website and personal portfolio and some personal musings.

* Being a dev rocks, except in my case for when i have to work for myself. Then vuepress came to save my bacon and allowed me to put together something to share about myself. Kudos to vuepress to make my life easy!

## Powered By

[VuePress](https://vuepress.vuejs.org/): Vue-powered Static Site Generator

[Vue.js](https://vuejs.org/): The Progressive JavaScript Framework