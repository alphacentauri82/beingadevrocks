# About Me 👩‍🎤

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.5.1/katex.min.css">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/github-markdown-css/2.2.1/github-markdown.css"/>

## My Story

I don't have much to say, or better, i don't like talking about myself. However i hope this gives anyone a little inspiration to never give up and if it is your desire to become a developer or devops, hope you find my story encouraging.

i'm 35 years old. Always loved coding and infrastructure. I started at the age of 12 surrounded of tech loving parents. However i always "thought" this was a hobby until i took it seriously and started working for freelance clients in my spare time. Today, this is what i do for a living.

To make a long story short, i did loads of online courses, and i am continually seeking to study and get certified. There's no going back and definitely requires ongoing training. Impostor syndrome kicks in from time to time and that's when one has to look back and see you're not the same person you were before learning all these things!. 


![rosie](http://culto.latercera.com/wp-content/uploads/2018/03/pg-38-rosie-riveter-1.jpg "Rosie the Riveter with sass! - Image by: ?") 
___

Nowadays i'm a full stack developer and i love javascript. I currently work as a DevOps/Developer and came to love front end development when i discovered Vue.js. It was just much easier for me and we are now in a long term relationship... sometimes i see other frameworks like React and Angular but, most important... i do what i like and what i think suits me best! - don't marry frameworks or libraries!. (though vue totally rocks!)


### Is it difficult to be a woman in tech?

Depends on how hard you make it for yourself. Truth is, there are loads of hurdles to overcome. I decided to take a better approach and contrinute as much as possible on helping building a community where we can learn and grow professionally in a safe and inclusive environment, this is why thanks to Jen Looper and Natalia Tepluhina i am part of Vue Vixens and also founder of Vue Vixens Argentina. 

For more information check: [Vue Vixens](https://vuevixens.org "Vue Vixens website!")

I decided that whatever difficulties come my way, that i will always remember: **WE CAN DO IT**
