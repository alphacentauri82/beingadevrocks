# Events 🚀

Workshops, Talks, Etc...

## Rompiendo Mitos @ Nardoz 

<MarkdownCard>

  **4th Jun 2018 - Mulesoft Argentina**
  
  Impostor syndrome and other important issues in our day to day lives as developers. Let's get those myths busted!


</MarkdownCard>

## Vue Vixens Argentina Presentation 

<MarkdownCard>

  **21st Jun 2018 - Stensul Argentina**
  
  Official presentation of Vue Vixens Argentina to the Dev community at Vuenos Aires' event.

  [[Recorded Stream](https://youtu.be/nzXkxBfiVEI?t=1h3m58s)] [[Slides](https://slides.com/superdiana/vuevixensar)]

</MarkdownCard>

## UPCOMING: VueJS London 

<MarkdownCard>

  **20-21 Sep 2018 - London. UK**
  
  * Deployment 101 for frontenders: All you need to know to ship your apps to the cloud!
  * Mentor @ Vue Vixens Skulk

[[Website](https://vuejs.london)]


</MarkdownCard>

## UPCOMING: Connect Tech Atlanta

<MarkdownCard>

  **17-19 Oct 2018 - Atlanta. GA, USA**
  
  * New to front end dev? - Get rocking with Vue.Js: From a DevOps & Backend perspective, an honest, quick and easy approach to get on front end development with confidence!.
  * Mentor @ Vue Vixens Skulk

 [[Website](https://connect.tech)]

</MarkdownCard>
