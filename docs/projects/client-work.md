# Client Work 💰
 
Freelance/Client work.

## TVQuark

<MarkdownCard image="/projects/quark.png">

  **Client** : Estan Group LLC | **App Name**: TVQuark | **Year**: 2016 
  
  **Role**: SysAdmin / Full Stack Dev
  
  **Specs:**

  Along with Mangoo Dev. Backend: Java/MVN Springboot. Front: JHipster. NGINX-RTMP Streaming engine for private subscriptions. Specific streaming security protocols. Infrastructure and CI/CD . 
  
  [[DEMO](https:tvquark.com)] 

</MarkdownCard>

## Cinéfilos Oficial

<MarkdownCard image="/projects/cinefilos.png">

  **Client** : Estan Group LLC | **App Name**: TVQuark | **Year**: 2016 
  
  **Role**: SysAdmin / Full Stack Dev
  
  **Specs:**

  Along with Mangoo Dev. Backend: Java/MVN Springboot. Front: JHipster. NGINX-RTMP Streaming engine for private subscriptions. Specific streaming security protocols. Infrastructure and CI/CD . 
  
  [[DEMO](https:tvquark.com)] 

</MarkdownCard>

## BiciKaiser

<MarkdownCard image="/projects/kaiser.png">

  **Client** : BiciKaiser | **App Name**: Point of Sale / Repair Shop | **Year**: 2018 
  
  **Role**: Full Stack Dev
  
  **Specs:**

  Developed a word press based point of sale. Registers & Cashier control. Linked to woocommerce to process offline sales (in store). Repair Shop system for repair orders, estimates and deliveries. Includes stock control and also linked to the main POS / woocommerce for stock control.
  
  [[DEMO UNAVAILABLE]()] 

</MarkdownCard>

