# Public Projects 💖

Community contributions, open source projects, initiatives, etc.

<MarkdownCard image="/projects/vixens.png">

  **LATAM Regional Leader @ Vue Vixens | Vue Vixens Argentina founder**  
  
  Apr 2018 - Present
  
  Vue Vixens is an initiative founded by Jen Looper, Developer Advocate at Progress, that creates and hosts workshops to teach Vue.js to under-represented people in a cool and fun way. It is based on the successful model pioneered by Shmuela Jacobs for the Angular community [[n-girls.org](https://ng-girls.org)] who was in turn inspired by the Rails Bridge and Django Girls initiatives. The format involves self-driven code labs completed in a workshop format in groups with mentoring by conference-goers and speakers who volunteer to help. The goal of the program is to familiarize women and those who identify as such with Vue.js in a supportive and inclusive environment. We also help conferences organize their diversity initiatives such as offering free conference tickets, and are building a scholarship fund to help offset those attendees' conference costs so that they can more easily attend. 
  
  Questions? Contact us at info@vuevixens.org / argentina@vuevixens.org

  [[Official Website](https://vuevixens.org)]

</MarkdownCard>