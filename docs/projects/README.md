# Workshop Materials 🌈
 
Updated as often as possible!

## Swag-O-Meter (React / Vue)

<MarkdownCard image="/projects/swagometer.png">

  Level: All levels
  
  **Get your Swag with React/VUE**
  
  Simple app inspired by "RitmoSustanciometro". For a beginner workshop on React / Styled Components || Vue.js and easy ways to deploy!. Development is fun, and it's a lot more when we learn to deploy our work. #Swag2Cloud
  
  [[DEMO](https:swag-o-meter.surge.sh)] [[React Version](https://github.com/alphacentauri82/swagometer-react)] [[Vue Version](https://github.com/alphacentauri82/swagometer-vue)]

</MarkdownCard>

## Hello Vue! The quickest Adele app ever!

<MarkdownCard image="/projects/adele.png">

  Level: All levels
  
  **Hello...**
  
  A revamp of Goncy Pozzo's Hello!. A quick and easy way to make a funny Vue app. Beginner workshop material. 
  
  [[DEMO](https:adelehellovue.surge.sh)] [[Repo](https://github.com/alphacentauri82/adelehellovue)] 

</MarkdownCard>

## Hacker News Clone

<MarkdownCard image="/projects/hackernews.png">

  Level: All levels
  
  **Hacker News Clone in NextJS + React Styled Components**
  
  [[Garnet Approves this app!](https://hackernews-ac82-eklrgrvwxt.now.sh/about)]. NEXTJS Workshop. Simple HackerNews Clone. 
  
  [[DEMO](https://hackernews-ac82-eklrgrvwxt.now.sh)] [[Repo](https://github.com/alphacentauri82/HackerNews-NextJS)] 

</MarkdownCard>