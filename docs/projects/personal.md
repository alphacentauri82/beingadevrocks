# Personal Projects 💅

Random stuff i've made that doesn't fit in the workshop category. Including course projects etc.

## Crystal Gem CV Template

<MarkdownCard image="/projects/cvonline.png">

  Level: All levels
  
  **Garnet's CV (HTML + CSS)**
  
  A cool cv resembling a code editor in Ubuntu 17.
  
  [[DEMO](https://alphacentauri82.github.io/cvonline/)] [[Repo](https://github.com/alphacentauri82/swagometer-react)] 

</MarkdownCard>

## Rompecabezas

<MarkdownCard image="/projects/rompecabezas.png">

  Level: All levels
  
  **Interactive Puzzle**
  
  Crack this easy puzzle in less than 80 moves. Simple Html/css/js.
  
  [[Repo](https://github.com/alphacentauri82/rompecabezas)] 

</MarkdownCard>

## Pixel Art

<MarkdownCard image="/projects/pixelart.png">

  Level: All levels
  
  **Get Creativity Flowing**
  
  Draw your favourite superhero!
  
  [[Repo](https://github.com/alphacentauri82/rompecabezas)] 

</MarkdownCard>

## Ciudad Zombie

<MarkdownCard image="/projects/ciudadzombie.png">

  Level: All levels
  
  **Interactive Puzzle**
  
  Crack this easy puzzle in less than 80 moves. Simple Html/css/js.
  
  [[Repo](https://github.com/alphacentauri82/rompecabezas)] 

</MarkdownCard>