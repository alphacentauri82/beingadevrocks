module.exports = {
  title: "Being a Dev Rocks! 🦄",
  description: "It's awesome, innit?",
  head: [["link", { rel: "icon", type:"image/png", href: `/logo.png` }]],
  base: "/",
  dest: "./dist",

  themeConfig: {
    nav: [
      { text: "Home", link: "/" },
      { text: "About", link: "/about/" },
      { text: "Projects", link: "/projects/" },
      { text: "GitHub", link: "https://github.com/alphacentauri82/" }
    ],
    sidebar: {
      '/projects/': genSidebarConfig('Projects')
    },
  },

  markdown: {
    // options for markdown-it-anchor
    anchor: { permalink: false },
    config: md => {
      md.use(require("markdown-it-katex"));
    }
  }
};

function genSidebarConfig (title) {
  return [
    {
      title,
      collapsable: false,
      children: [
        '',
        'client-work',
        'public-projects',
        'personal',
        'events',
      ]
    }
  ]
}


