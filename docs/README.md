---
layout: Homepage
description: 'Home'
avatar: /diana.jpg
head: 'SuperDiana'
info: 'Proud Vue Vixen 🦊 | Fullstack Dev / DevOps 👩‍🎤'
interests: 'Contact && My Top Bookmarks'
socials:
- title: github
  link: https://github.com/alphacentauri82
- title: linkedin
  link: https://www.linkedin.com/in/nerdattack82
- title: twitter
  link: https://twitter.com/cotufa82
actions: 
- text: Vue Vixens 🦊
  link: https://vuevixens.org
- text: Vuenos Aires 🇦🇷
  link: http://vuenosair.es
- text: MeetupJS 🇦🇷
  link: http://meetupjs.com.ar/
- text: FreeCodeCampBA 🇦🇷
  link: https://freecodecampba.org/
- text: SysArmy 🇦🇷  
  link: https://sysarmy.com.ar/
footer: Made with ♥ by me, thanks to Fing for his awesome Harry Potter theme! - Powered by VuePress
---

Being DevOps/SysAdmin totally rocks. Being a developer rocks!. Being a Vue Vixen rocks my socks. But why?, don't we all suffer at some point? - probably on a daily basis i am seen arguing with my computer and also cheering when things work. It hasn't changed since day 1. You won't see a lot of swanky things but here i share my projects and all the reasons that make me love doing what i do! :purple_heart:
